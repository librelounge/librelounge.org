title: 31: Society and Free Software with Molly de Blanc
date: 2020-01-25 00:00:00
tags: episode, free software, open source
summary: 31: Society and Free Software with Molly de Blanc
enclosure: title:ogg url:"https://media.librelounge.org/episodes/031-society-with-mollydeblanc/librelounge-ep-031.ogg" length:"21812160" duration:"00:54:20"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/031-society-with-mollydeblanc/librelounge-ep-031.mp3" length:"35517971" duration:"00:54:20"
---
In this episode, Serge sits down with Molly de Blanc, long time Free Software activist and leader, now president of the Open Source Initiative about the intersection of Free Software and society, and where we as a community are moving.

Links:
- [Molly de Blanc's Blog](http://deblanc.net/blog/)
- [Article about Detroit's Tree Project](https://www.citylab.com/environment/2019/01/detroit-tree-planting-programs-white-environmentalism-research/579937/)
- [Libre Lounge episode about Cypurr](https://librelounge.org/episodes/28-cypurr-with-rorym.html)
- [Libre Lounge episode about Free Culture movements](https://librelounge.org/episodes/episode-7-free-remix-and-youtube-culture.html)


