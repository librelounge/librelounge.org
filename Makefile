BASEDIR=$(CURDIR)
OUTPUTDIR=$(BASEDIR)/site
GUIX=$(which guix)
HAUNT_ENVIRONMENT='guix environment -l guix.scm'

SSH_HOST=drippy.emacsen.net
SSH_PORT=22
SSH_USER=librelounge
SSH_TARGET_DIR=/srv/docker/librelounge/html

help:
	@echo 'Makefile for Libre Lounge                                                 '
	@echo '                                                                          '
	@echo 'Usage:                                                                    '
	@echo '   make html                           (re)generate the web site          '
	@echo '   make clean                          remove the generated files         '
	@echo '   make devserver                      start the development server (8080),
	@echo '   make ssh_upload                     upload the web site via SSH        '
	@echo ,   make environment                    set up haunt environment           ,
	@echo '                                                                          '
	@echo 'Set the DEBUG variable to 1 to enable debugging, e.g. make DEBUG=1 html   '
	@echo 'Set the RELATIVE variable to 1 to enable relative urls                    '
	@echo '                                                                          '


haunt_environment:
	guix environment -l guix.scm

html:
	guix environment -l guix.scm -- haunt build

clean:
	[ ! -d $(OUTPUTDIR) ] || rm -rf $(OUTPUTDIR)

devserver: html
	guix environment -l guix.scm -- haunt serve --watch

ssh_upload:
	scp -P $(SSH_PORT) -r $(OUTPUTDIR)/* $(SSH_USER)@$(SSH_HOST):$(SSH_TARGET_DIR)


