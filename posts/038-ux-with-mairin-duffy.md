title: 38: User Interfaces with Máirín Duffy
date: 2020-05-15 21:02
tags: episode
summary: User Interfaces with Máirín Duffy
enclosure: title:ogg url:"https://media.librelounge.org/episodes/038-ux-with-mairin-duffy/librelounge-ep-038.ogg" length:"33239914" duration:"00:55:54"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/038-ux-with-mairin-duffy/librelounge-ep-038.mp3" length:"30612836" duration:"00:55:54"
---
In this episode, Chris and Serge talk with User Interface expert Máirín Duffy about her history with Free Software, user interface in modern GNU/Linux systems and what developers can do to make better programs.

Links
- [Máirín's Blog](https://mairin.wordpress.com)
