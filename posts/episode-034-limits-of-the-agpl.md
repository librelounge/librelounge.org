title: 34: The Limits of the AGPL
date: 2020-04-03 18:00:00
tags: episode, gaming, license, agpl
summary: 34: The Limits of the AGPL
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/034-limits-of-agpl/librelounge-ep-034.mp3" length:"29912976" duration:"00:59:22"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/034-limits-of-agpl/librelounge-ep-034.ogg" length:"35771080" duration:"00:59:22"
---
Serge and Chris briefly discuss space games before a deep dive concern about the Affero General Public 
License.

Links:
- [The Affero General Public License](https://www.gnu.org/licenses/agpl-3.0.en.html)
- [Is the AGPL Broken?](https://write.emacsen.net/breaking-the-agpl)
- [Empty Epsilon](http://daid.github.io/EmptyEpsilon/)
