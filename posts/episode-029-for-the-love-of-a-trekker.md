title: 29: For the love of a Trekker
date: 2019-12-13 16:45:00
tags: star trek
summary: 29: For the love of a Trekker
enclosure: title:ogg url:"https://media.librelounge.org/episodes/029-for-the-love-of-a-trekker/librelounge-ep-029.ogg" length:"5033465" duration:"00:12:00"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/029-for-the-love-of-a-trekker/librelounge-ep-029.mp3" length:"3988760" duration:"00:12:00"
---
On this mini-episode of Libre Lounge, Serge talks about his friend Greg Schnitzer, how the values of Star Trek made an impact on his life, and what we in the User Freedom community can learn from them.

Links:
- [Star Trek New Voyages](https://en.wikipedia.org/wiki/Star_Trek:_New_Voyages)
- [New Voyages Episodes](https://vimeo.com/startreknewvoyages)
- [Video of Greg Talking about Props](https://www.youtube.com/watch?v=6AuJXAy-PAs)
- [An online tribute to Greg](https://www.aapc.com/blog/40067-look-for-gregory-l-schnitzer-in-the-stars/)
- [Trekkies Documentary](https://en.wikipedia.org/wiki/Trekkies_(film))
- [For the Love of Spock](https://en.wikipedia.org/wiki/For_the_Love_of_Spock)
