title: 32: Companies, Money and Society with Frank Karlitschek
date: 2020-02-28 18:30:00
tags: episode, companies, money, free software, federation
summary: 32: Companies, Money and Society with Frank Karlitschek
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/032-companies-with-frank-karlitschek/librelounge-ep-032.mp3" length:"43766112" duration:"01:04:31"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/032-companies-with-frank-karlitschek/librelounge-ep-032.ogg" length:"34880154" duration:"01:04:31"
---
In this episode, Serge sits down with Frank Karlitschek, the founder and president of Nextcloud about why Frank forked his own company, the influence of money in software companies and the necessity of federation in the future of computing.

Links:
- [Frank Karlitschek's Website](https://karlitschek.de/)
- [Nextcloud](https://nextcloud.com/)
- [Why I forked my own project and my own company](https://media.libreplanet.org/u/libreplanet/m/why-i-forked-my-own-project-and-my-own-company-31c3/)
- [Why the GPL is great for business](https://fosdem.org/2020/schedule/event/gpl_and_business/)
- [Red Hat announced that they've joined forces with CentOS](https://www.redhat.com/en/about/press-releases/red-hat-and-centos-join-forces)
- [The Creator-Endosed Mark](https://questioncopyright.org/creator-endorsed-mark)
- [The Esperanto Language](https://en.wikipedia.org/wiki/Esperanto)
- [Blisssymbolics](https://en.wikipedia.org/wiki/Blissymbols)
- [Khan Academy (the website that Serge couldn't remember the name of)](https://www.khanacademy.org/)
