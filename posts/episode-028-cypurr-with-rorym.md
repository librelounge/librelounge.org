title: 28: CyPurr with RoryM
date: 2019-09-27 00:00:00
tags: episode, cypurr, communication, privacy, security
summary: 28: CyPurr with RoryM
enclosure: title:ogg url:"https://media.librelounge.org/episodes/028-cypurr-with-rorym/librelounge-ep-28.ogg" length:"15382528" duration:"00:27:38"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/028-cypurr-with-rorym/librelounge-ep-28.mp3" length:"28786936" duration:"00:27:38"
---
On this episode of Libre Lounge, Chris and Serge are joined by RoryM of 
the CyPurr Collective, a Brooklyn based digital security group focused 
on bringing improved computer privacy and security to the masses through 
workshops that focus on asking questions and listening.

Hear about CyPurr's approach, its history and its vision, as well as 
what we in the User Freedom community can learn about how to better 
connect with everyday people.


Links:
- [CyPurr Collective](https://cypurr.nyc/)
- [CyPurr's Github Page](https://github.com/CyPurr-Collective/)
- [RoryM on the Fediverse](https://anticapitalist.party/@falsemirror)
- [RoryM on Twitter](https://twitter.com/_falsemirror)
- [Holistic Security Manual](https://holistic-security.tacticaltech.org/)
- [Blue Stockings Bookstore](http://bluestockings.com/)
- [The EFF's Security Education Companion](https://sec.eff.org)
