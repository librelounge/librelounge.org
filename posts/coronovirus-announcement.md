title: Coronovirus Announcement
date: 2020-03-17 01:00:00
tags: coronovirus
summary: Coronovirus Announcement
enclosure: title:ogg url:"https://media.librelounge.org/episodes/coronovirus-announcement/ll-coronovirus-announcement.ogg" length:"913156" duration:"00:01:35"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/coronovirus-announcement/ll-coronovirus-announcement.mp3" length:"880506" duration:"00:01:35"
---
Serge has some personal words about Coronovirus, and how Libre Lounge is responding.
