title: 35: COVID and EARN-IT with Sean O'Brien
date: 2020-04-08 15:10
tags: episode, covid, earn-it, privacy
summary: COVID and EARN-IT with Sean O'Brien
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/035-earnit-with-sean-obrien/librelounge-ep-035.mp3" length:"23945025" duration:"00:45:56"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/035-earnit-with-sean-obrien/librelounge-ep-035.ogg" length:"28388287" duration:"00:45:56"
---
CW: This episode has dark themes and may be challenging for those suffering from depression

In this episode of Libre Lounge, Serge and Chris invite Sean O'Brien back to the show to talk about the threats to our privacy that have arisen during the COVID-19 pandemic, including and especially the EARN-IT act, as well as how tragedies have effected societies in the past.

Links:
- [The Previous Episode with Sean](https://librelounge.org/episodes/episode-15-at-libre-planet-with-sean-obrien.html)
- [The EARN-IT Act](https://www.eff.org/deeplinks/2020/03/earn-it-act-violates-constitution)
- [SESTA/FOSTA](https://stopsesta.org/)
- [The Australian Decryption Law Panel from Libre Planet 2019](https://media.libreplanet.org/u/libreplanet/m/australia-s-decryption-law-and-free-software/)
- [The Privacy Issue](https://theprivacyissue.com/)
- [MakeHaven](https://www.makehaven.org/)
- [Privacy Safe](https://privacysafe.ai/)
- [Flatten the Curve](https://flattenthecurve.tech)
- [GNU Health](https://www.gnuhealth.org/)
