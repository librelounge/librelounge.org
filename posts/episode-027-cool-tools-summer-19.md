title: 27: Cool Tools of Summer '19
date: 2019-09-20 00:00:00
tags: episode, command line, tools
summary: 27: Cool Tools of Summer '19 
enclosure: title:ogg url:"https://media.librelounge.org/episodes/027-cool-tools-summer-19/librelounge-ep-027.ogg" length:"11491445" duration:"00:23:23"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/027-cool-tools-summer-19/librelounge-ep-027.mp3" length:"8513374" duration:"00:23:23"
---
It's been a long summer and it's time for a fun break. Serge and Chris 
talk about some of their favorite fun tools in this short episode of 
Libre Lounge!

Links:
- [htop](https://hisham.hm/htop/)
- [figlet](http://www.figlet.org/)
- [byobu](http://byobu.co/)
- [Ledger](https://plaintextaccounting.org/)
- [mosh](https://mosh.org/)
- [autossh](https://www.harding.motd.ca/autossh/)
- [unison](https://www.cis.upenn.edu/~bcpierce/unison/)
- [baobab](https://wiki.gnome.org/Apps/DiskUsageAnalyzer)
- [Pandoc](https://pandoc.org/)
- [bb](http://aa-project.sourceforge.net/bb/)
- [watch](http://www.linfo.org/watch.html)
- [Cool Retro Term](https://github.com/Swordfish90/cool-retro-term)
