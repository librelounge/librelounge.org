title: 25: Managing Spam and Hate-Speech on the Fediverse
date: 2019-08-02 00:00
tags: episode, activitypub, ocap, spam
summary: 25: Managing Spam and Hate-Speech on the Fediverse
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/025-fediverse-spam/librelounge-ep-025.mp3" length:"23688677" duration:"00:59:31"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/025-fediverse-spam/librelounge-ep-025.ogg" length:"35704277" duration:"00:59:31"
---
Serge and Chris have been busy working on a plan to keep the Fediverse safe
from spam, scammers and hate speech. On this episode of Libre Lounge, they
share that plan with the world.

We also announce the [ActivityPub
Conference](http://dustycloud.org/blog/activitypub-conf-2019/) for September
7th and 8th!

Links:

- Serge's Paper [Keeping Unwanted Messages off the Fediverse](https://github.com/emacsen/rwot9-prague/blob/ap-unwanted-messages/topics-and-advance-readings/ap-unwanted-messages.md)
- Chris's Paper [OcapPub: Towards networks of consent](https://gitlab.com/spritely/ocappub/blob/master/README.org) 
- Our show on [Object Capabilities](https://librelounge.org/episodes/episode-13-object-capabilities-with-kate-sills.html)
- Our relevant show on [ActivityPub](https://librelounge.org/episodes/episode-12-activitypub-part-1.html)
- [ActivityPub Conf](http://dustycloud.org/blog/activitypub-conf-2019)
