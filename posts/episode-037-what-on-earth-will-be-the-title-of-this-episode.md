title: 37: What on Earth will be the title of this epsiode?
date: 2020-05-03 14:45
tags: episode
summary: What on Earth will be the title of this episode?
enclosure: title:ogg url:"https://media.librelounge.org/episodes/037-what-on-earth-will-be-the-title/librelounge-ep-037.ogg" length:"34624608" duration:"00:58:22"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/037-what-on-earth-will-be-the-title/librelounge-ep-037.mp3" length:"29066973" duration:"00:58:22"
---
Serge and Chris have an conversation about a variety of topics including gender, real vs virtual life, language and pronouns.


