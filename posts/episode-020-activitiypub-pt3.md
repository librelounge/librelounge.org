title: Episode 20: ActivityPub Part 3
date: 2019-05-17 13:00:00
tags: episode, activitypub, programming 
summary: ActivityPub Part 3
enclosure: title:ogg url:"https://media.librelounge.org/episodes/020-activitypub-pt3/librelounge-ep-020.ogg" length:"31523502" duration:"00:52:52"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/020-activitypub-pt3/librelounge-ep-020.mp3" length:"21405192" duration:"00:52:52"
---
In this episode of Libre Lounge, Chris and Serge continue exploring 
ActivityPub. They use this episode to discuss webfinger, http signatures 
and some of the parts of the ActivityPub protocol that are less well 
understood.

Listen to Part 1 **[Here](https://librelounge.org/episodes/episode-12-activitypub-part-1.html)**

Listen to Part 2 **[Here](https://librelounge.org/episodes/episode-17-activitypub-part-2.html)**

Links:

- [ActivityPub](https://www.w3.org/TR/activitypub/)
- [Webfinger](https://webfinger.net/)
- [HTTP Signatures](https://tools.ietf.org/html/draft-cavage-http-signatures-10)
- [5 Day Wait time in SMTP](https://tools.ietf.org/html/rfc5321#section-4.5.4.1)
- [ActivityPub Federation, Deniel of Service](https://www.w3.org/TR/activitypub/#security-federation-dos)

