title: Episode 9: Funding Free Software Development (pt2)
date: 2019-01-25 01:37
tags: episode, free software
summary: Free Software, Funding
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/009-fundraising-pt2/librelounge-ep-009.mp3" length:"35173172" duration:"01:43:16"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/009-fundraising-pt2/librelounge-ep-009.ogg" length:"38473725" duration:"01:43:16"
---
Chris and Serge continue their discussion from last time about Free Software funding models.

Listen to Part 1 [here](https://librelounge.org/episodes/episode-8-funding-free-software-development-pt1.html)
