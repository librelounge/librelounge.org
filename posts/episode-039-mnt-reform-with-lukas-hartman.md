title: 39: MNT Reform with Lukas Hartmann
date: 2020-05-27 34:50
tags: episode
summary: MNT Reform with Lukas Hartmann
enclosure: title:ogg url:"https://media.librelounge.org/episodes/039-mnt-reform-with-lukas-hartmann/librelounge-ep-039.ogg" length:"26327246" duration:"00:43:29"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/039-mnt-reform-with-lukas-hartmann/librelounge-ep-039.mp3" length:"22828025" duration:"00:43:29"
---
The MNT Reform is a Free Hardware laptop that entirely changes the way laptops are desgined and used. Come with us as we talk with the inventor, Lukas Hartmann about his journey into Free Software, the MNT's development and what the future holds on this episode of Libre Lounge!

Links

- [MNT Reform Crowdsupply (get your own!)](https://www.crowdsupply.com/mnt/reform)
- [MNT Reform](https://mntre.com/)
- [Genode Operating System](https://genode.org/)
