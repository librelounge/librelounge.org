title: 36: David Revoy on Pepper & Carrot and Free Culture
date: 2020-04-17 16:30
tags: episode, free culture
summary: David Revoy on Pepper & Carrot and Free Culture
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/036-devid-revoy-pepper-and-carrot/librelounge-ep-036.mp3" length:"23723231" duration:"00:45:49"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/036-devid-revoy-pepper-and-carrot/librelounge-ep-036.ogg" length:"27977698" duration:"00:45:49"
---
Serge and Chris sit down with award winning artist and Free Culture activist David Revoy on his webcomic series Pepper and Carrot, the Sintel film, and how he started his Free Software/Free Culture journey.

Links:
[David Revoy's Website](https://www.davidrevoy.com)
[Supporting David Financially](https://www.davidrevoy.com/static3/become-my-patron)
[Pepper & Carrot](https://www.peppercarrot.com/)
[Sintel Film](https://cloud.blender.org/p/sintel/)
[3DVF](https://www.3dvf.com/)
[Framasoft](https://framasoft.org)
[Blender Foundation](https://www.blender.org/foundation/)
[Blender Institute](https://www.blender.org/institute/)
[Krita](https://krita.org/)
[MyPaint](http://mypaint.org/)
[GNUess (the cartoon David made live at LibrePlanet 2020)](https://www.davidrevoy.com/article763/gnuess)
