title: Episode 24: Hackers and Hackerspaces with Mitch Altman
date: 2019-06-28 15:10
tags: episode, hackers, hackerspaces, depression, community
summary: Hackers and Hackerspaces with Mitch Altman
enclosure: title:ogg url:"https://media.librelounge.org/episodes/024-hackers-mitch-altman/librelounge-ep-024.ogg" length:"39362266" duration:"01:04:41"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/024-hackers-mitch-altman/librelounge-ep-024.mp3" length:"73225322" duration:"01:04:41"
---
What are Hackers? What are Hackerspaces? What makes us feel a sense of purpose and belonging? What are the origins of the term Open Source and Maker, and what do they have to do with one another? Mitch Altman brings clarity on all these topics and more when he joins Chris and Serge for this episode of Libre Lounge!

Links:

- [Cornfield Electronics (Mitch's website)](https://cornfieldelectronics.com)
- [TV-B-Gone](https://www.tvbgone.com/)
- [Hackerspaces.org](https://hackerspaces.org/)
- [2600 Magazine](https://2600.com/)
- [Off the Hook Radio Show](https://www.2600.com/node/17585)
- [Chaos Communication Club](https://www.ccc.de)
- [Novena Computer](https://www.crowdsupply.com/sutajio-kosagi/novena)
- [Soldering is Easy](https://mightyohm.com/files/soldercomic/FullSolderComic_EN.pdf)
