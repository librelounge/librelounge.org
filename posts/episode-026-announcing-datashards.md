title: 26: Announcing Datashards
date: 2019-08-16 20:00:00
tags: episode, datashards, encryption, privacy
summary: 26: Announcing Datashards
enclosure: title:ogg url:"https://media.librelounge.org/episodes/026-announcing-datashards/librelounge-ep-26.ogg" length:"24419485" duration:"00:47:32"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/026-announcing-datashards/librelounge-ep-26.mp3" length:"53987856" duration:"00:47:32"
---
Chris and Serge have an exciting announcement in the form of a new project called Datashards!

Datashards is a collaborative secure communication system that can be used online or offline and solves problems such as the Slashdot effects, offline data retrival, secure communication and more. Chris and Serge walk through the basic features of DataShards and why they're both working on it!

Links:
- [The DataShards Website](https://datashards.net)
- [Tahoe-LAFS](https://tahoe-lafs.org)
- [Freenet](https://freenetproject.org)
- [Jason Scott Talking about the Death of Geocities and Internet Culture](https://www.youtube.com/watch?v=-2ZTmuX3cog)
- [Definition of the Slashdot Effect](https://en.wikipedia.org/wiki/Slashdot_effect)
- [IPFS](https://ipfs.io/)
- [The original Freenet Paper](https://freenetproject.org/papers/ddisrs.pdf)
