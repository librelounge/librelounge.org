title: 30: Fennel with Phil Hagelberg
date: 2019-12-19 00:00:00
tags: episode, programming, lisp, fennel, lua, games, hardware, keyboard
summary: 30: Fennel with Phil Hagelberg
enclosure: title:ogg url:"https://media.librelounge.org/episodes/030-fennel-with-technomancy/librelounge-ep-030.ogg" length:"25734982" duration:"00:55:36"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/030-fennel-with-technomancy/librelounge-ep-030.mp3" length:"20786360" duration:"00:55:36"
---
Phil Hagelberg (technomancy) joins Serge for a conversation about Clojure, a new lisp called Fennel, making video games, and making free hardware design keyboards.

Links:
- [Fennel Language Website](https://fennel-lang.org/)
- [Atreus Keyboard](https://atreus.technomancy.us/)
- [Phil Hagelberg's Website](https://technomancy.us/)
- [Phil Talking about the creation of the Atreus Keyboard](https://www.youtube.com/watch?v=Mfyggktkox4)
- [Leiningen](https://leiningen.org/)
- [Energize! Game](https://technomancy.itch.io/energize)
- [EXO_encounter 667 Game](https://technomancy.itch.io/exo-encounter-667)
- [The Lua Language](https://www.lua.org/)
- [Open Source is Not About You, Rich Hickey's essay about community](https://gist.github.com/richhickey/1563cddea1002958f96e7ba9519972d9)
