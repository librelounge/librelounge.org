title: 33: Chris's Return, Covid and Social Gaming
date: 2020-03-26 14:50:00
tags: episode, gaming, social distancing
summary: 33: Chris's Return, Covid and Social Gaming
enclosure: title:ogg url:"https://media.librelounge.org/episodes/033-chris-coronovirus-gaming/librelounge-ep-033.ogg" length:"27660602" duration:"00:46:27"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/033-chris-coronovirus-gaming/librelounge-ep-033.mp3" length:"23171031" duration:"00:46:27"
---
Chris returns to Libre Lounge for a casual conversation about their work, gender identity and social gaming during the era of COVID-19.

Links:
- [Spritely Project](https://gitlab.com/spritely)
- [Software Freedom Conservancy Greeting Card](https://sfconservancy.org/blog/2020/jan/17/fundraisingthx/)
- [Terminal Phase](https://dustycloud.org/blog/terminal-phase-1.1-and-goblins-0.6/)
- [Libre Lounge Episode about Cool Terminal Tools](https://librelounge.org/episodes/27-cool-tools-of-summer-19.html)
- [What is Non-Binary](https://www.healthline.com/health/transgender/nonbinary)
- [Spivak Pronouns](https://en.wikipedia.org/wiki/Spivak_pronoun)
- [Empty Epsilon](http://daid.github.io/EmptyEpsilon/)
