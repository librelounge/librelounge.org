title: Episode 19: Community Development with Deb Nicholson
date: 2019-05-10 00:00:00
tags: episode, community development,
summary: Community Development with Deb Nicholson
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/019-community-development/librelounge-ep-019.mp3" length:"26800824" duration:"01:08:44"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/019-community-development/librelounge-ep-019.ogg" length:"40210304" duration:"01:08:44"
---
Serge sits down with both Chris and Deb Nicholson to discuss building and maintaining a healthy Free Software community.

Show Links:

- [GNU Mediagoblin](https://mediagoblin.org/)
- [Software Freedom Conservancy](https://sfconservancy.org/)
- [Spinach Con](https://opensource.com/life/16/4/spinachcon)
- [Contributor Covenant](https://www.contributor-covenant.org/)
- [OpenHatch](https://openhatch.org/)
- [First Timers Only](https://www.firsttimersonly.com/)
- [OpenBSD Release Songs](https://www.openbsd.org/lyrics.html)
