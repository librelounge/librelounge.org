title: Episode 21: Command Line Culture
date:date: 2019-05-31 10:00:00
tags: episode, hacker culture
summary: Command Line Culture
enclosure: title:ogg url:"https://media.librelounge.org/episodes/021-commandline-culture/librelounge-ep-021.ogg" length:"23095566" duration:"00:38:40"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/021-commandline-culture/librelounge-ep-021.mp3" length:"44357129" duration:"00:38:40"
---
Chris and Serge take a walk down memory lane of old computers, programming environments and the command line. During this trip, they deconstruct the appeal of the command line, unix culture and ways that the aesthetics of the terminal have held us back.

Links:

- [Commodore VIC-20 (wikipedia)](https://en.wikipedia.org/wiki/Commodore_VIC-20)
- [Commodore 64 (wikipedia)](https://en.wikipedia.org/wiki/Commodore_64)
- [MS DOS (wikipedia)](https://en.wikipedia.org/wiki/MS-DOS)
- [Freedos (a Free Software DOS replacement)](https://www.freedos.org/)
- [QBasic (wikipedia)](https://en.wikipedia.org/wiki/QBasic)
- [FreeBASIC (a Free Software BASIC)](https://www.freebasic.net/)
- [GEOS (wikipedia)](https://en.wikipedia.org/wiki/GEOS_(8-bit_operating_system))
- [Unix Philosophy (wikipedia)](https://en.wikipedia.org/wiki/Unix_philosophy)
- [REPL (wikipedia)](https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop)
- [In the Beginning... Was the Command Line (wikipedia)](https://en.wikipedia.org/wiki/In_the_Beginning..._Was_the_Command_Line)
- [8-Bit Guy's Dream Computer (the8bitguy.com)](http://www.the8bitguy.com/2576/what-is-my-dream-computer/)
- [Adafruit Circuit Playground Express (learn.adafruit.com)](https://learn.adafruit.com/adafruit-circuit-playground-express/overview)
- [Microsoft MakeCode (github.com)](https://github.com/microsoft/pxt)
- [Scratch](https://scratch.mit.edu/)
- [The UNIX-HATERS Handbook](https://web.mit.edu/~simsong/www/ugh.pdf)
- [The Mycroft Personal Voice Assistant](https://mycroft.ai/)
- [The Blender Project](https://www.blender.org/)
